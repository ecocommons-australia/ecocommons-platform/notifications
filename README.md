# Platform Service Notifications Banner

This is a simple repo that hosts a static page which can contain 'service notification' style messages.
These are displayed on the header of the platform.

## Instructions

Edit the following document to add a message.  Anything added will be rendered.

Please adhere to the following:
- Keep it simple and short!
- Only the most basic formatting.  Bold, A tag if required to link to an external resource (make sure you use `target="_blank"`)

```
pages/index.html
```

When no message is needed, replace the file contents with whitespace 
(at least one space is required for Gitlab Pages to publish it).

## Configuration

This is implemented via a component the UI-Library (`<MOTD />`) and configured in the deployment `frontend/shared/node.env`.

```
NEXT_PUBLIC_MOTD_URL=
```


## TODO

Improve this process with either structured data, ie json.  
Or more it entirely to the User Management API and build a dashboard 